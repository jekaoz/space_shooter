// import Phaser from 'phaser';

import BaseInteractiveUnit from '../components/BaseInteractiveUnit';
import Bullet from '../components/Bullet';
import Hazad from '../components/Hazard';
import {Layers} from '../typings/game';


const resolveCollisions = (
    o1: BaseInteractiveUnit,
    o2: BaseInteractiveUnit,
): void => {
    // Коллизия PlayerBullet - Hazard
    if (o1.layer === Layers.Bullet) {
        // Чтобы пуля могла поразить двух врагов, если задевает их одновременно
        setTimeout(() => {
            (o1 as Bullet).hurt(1);
        }, 0);

        (o2 as Hazad).hurt((o1 as Bullet).power);
    }
};

export default resolveCollisions;
