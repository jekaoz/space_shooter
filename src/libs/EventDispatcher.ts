import Phaser from 'phaser';


const EVENT_DISPATCHER_KEY = 'EVENT_EMITTER_INSTANCE';

export default class EventDispatcher extends Phaser.Events.EventEmitter {
    constructor() {
        const instance = (window as {[key: string]: any})[EVENT_DISPATCHER_KEY];

        if ((window as {[key: string]: any})[EVENT_DISPATCHER_KEY]) {
            return instance;
        }

        super();

        (window as {[key: string]: any})[EVENT_DISPATCHER_KEY] = this;
    }
}
