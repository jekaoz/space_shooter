import Phaser from 'phaser';

import BootScene from './scenes/BootScene';
import L1Scene from './scenes/L1Scene';

import {getGameConfig} from './utils';
import {staticConfig} from './constants/common';

import './index.scss';

const config = getGameConfig(staticConfig, [
    BootScene,
    L1Scene,
]);

// eslint-disable-next-line
new Phaser.Game(config);
