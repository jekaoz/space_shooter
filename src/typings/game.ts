export enum Layers {
    Player = 'player',
    Bullet = 'bullet',
    FlyingHazard = 'flying_hazard',
    GroundHazard = 'ground_hazard',
    Loot = 'loot',
}

export interface IUnitConfig {
    x: number;
    y: number;
    speed: number;
    texture: string;
    frame?: string;
}
