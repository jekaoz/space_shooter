// import Phaser from 'phaser';


const DEFAULT_FONT_FAMILY = 'KemcoPixel, serif';

export const PI = 3.14159;

export const BLINK_DURATION = 50;

export const UI_DEPTH = 1;
export const UI_ALPHA = 0.3;
export const UI_ALPHA_ACTIVE = 0.5;

export const SCORE_TEXT_CONFIG = {
    fontFamily: DEFAULT_FONT_FAMILY,
    fontSize: 24,
};

export const HAZARD_POINTS_TEXT_CONFIG = {
    fontFamily: DEFAULT_FONT_FAMILY,
    fontSize: 14,
};

export const EVENTS = {
    SET_SCORE: 'SET_SCORE',
    MOVE_STICK: 'MOVE_STICK',
    PRESS_X_BUTTON: 'PRESS_X_BUTTON',
};

export const ALIGN = {
    TOP: 'TOP',
    BOTTOM: 'BOTTOM',
    LEFT: 'LEFT',
    RIGHT: 'RIGHT',
};
