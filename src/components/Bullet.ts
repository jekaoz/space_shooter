import {Scene} from 'phaser';
import {staticConfig} from '../constants/common';
import BaseInteractiveUnit, {IBaseInteractiveUnit} from './BaseInteractiveUnit';
import {Layers} from '../typings/game';


export interface IBullet extends Omit<IBaseInteractiveUnit, 'layer' | 'health'> {
    power: number;
}

export default class Bullet extends BaseInteractiveUnit {
    power: number;

    constructor(scene: Scene, config: IBullet) {
        const {x, y, speed, texture, frame, power} = config;

        super(scene, {
            x,
            y,
            speed,
            texture,
            frame,
            health: 1,
            layer: Layers.Bullet,
        });

        this.power = power;

        this.x += this.width / 2;
    }

    isOutOfBounderies() {
        return this.y < -this.height || this.y > staticConfig.height + this.height;
    }

    update() {
        if (this.isOutOfBounderies()) {
            this.destroy();
        }
    }
}
