import {Scene} from 'phaser';
import Hazard, {IHazard} from './Hazard';


export default class BowHazard extends Hazard {
    constructor(scene: Scene, config: Omit<IHazard, 'health'>) {
        const {x, y, texture, speed, layer, frame, points} = config;

        super(scene, {
            x, y, texture,
            speed, layer, frame,
            points,
            health: 100,
        });

        this.play('bow_hazard');
    }
}
