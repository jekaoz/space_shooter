import Phaser, {Scene} from 'phaser';
import EventDispatcher from '../libs/EventDispatcher';
import PlayerBullet from './PlayerBullet';
import {IStickState} from './UI/Stick';
import {staticConfig} from '../constants/common';
import {EVENTS} from '../constants/game';
import {Layers} from '../typings/game';


/* TODO: Унести в конфиг игрока
- SPEED
- BULLET_COOLDOWN
- BULLET_SPEED */

export default class Player extends Phaser.GameObjects.Container {
    emitter: EventDispatcher;
    cursors: Phaser.Types.Input.Keyboard.CursorKeys;
    group: Phaser.GameObjects.Group;
    ship: Phaser.GameObjects.Sprite;
    flame: Phaser.GameObjects.Sprite;
    layer: Layers;
    speed: number;
    bulletSpeed: number;
    bulletPower: number;
    bulletCooldown: number;
    lastBulletTime: number;
    stickState: IStickState;
    xButtonPressed: boolean;

    constructor(scene: Scene, cursors: Player['cursors'], group: Player['group']) {
        super(scene, 150, 500);

        this.scene = scene;
        this.emitter = new EventDispatcher();
        this.cursors = cursors;
        this.group = group;
        this.layer = Layers.Player;
        this.speed = 300;
        this.bulletSpeed = 750;
        this.bulletCooldown = 500;
        this.bulletPower = 50;
        this.lastBulletTime = Date.now();
        this.xButtonPressed = false;
        this.stickState = {
            left: false,
            right: false,
            up: false,
            down: false,
        };

        this.createAnimations();
        this.createShip();
        this.createFlame();
        this.setEvents();

        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
        this.body.enable = true;
    }

    createAnimations() {
        this.scene.anims.create({
            key: 'ship_flame',
            frameRate: 10,
            repeat: -1,
            frames: this.scene.anims.generateFrameNames('ship_flame'),
        });
    }

    createShip() {
        this.ship = new Phaser.GameObjects.Sprite(this.scene, 0, 0, 'player_ship_1');
        this.ship.setOrigin(0.5, 0);
        this.width = this.ship.width;
        this.add(this.ship);
    }

    createFlame() {
        // technotes #1
        this.flame = this.scene.add.sprite(0, 0, 'ship_flame').play('ship_flame');
        this.flame.setOrigin(0.5, 0);
        this.flame.y = this.ship.height;
        this.add(this.flame);
    }

    setEvents() {
        this.emitter.on(EVENTS.MOVE_STICK, ({state, event}: {state: IStickState; event: string}) => {
            this.stickState = state;

            if (event === Phaser.Input.Events.DRAG_END) {
                this.body.setVelocity(0);
            }
        });

        this.emitter.on(EVENTS.PRESS_X_BUTTON, ({pressed}: {pressed: boolean}) => {
            this.xButtonPressed = pressed;
        });
    }

    checkBoundaries() {
        const leftBoundaryReached = this.x <= this.ship.width / 2;
        const rightBoundaryReached = this.x >= staticConfig.width - this.ship.width / 2;
        const upBoundaryReached = this.y <= 0;
        const bottomBoundaryReached = this.y >= staticConfig.height - this.ship.height - this.flame.height;

        return {
            leftBoundaryReached,
            rightBoundaryReached,
            upBoundaryReached,
            bottomBoundaryReached,
        };
    }

    moveShipLeft(leftBoundaryReached: boolean) {
        if (leftBoundaryReached) {
            this.x = this.ship.width / 2;

            return;
        }

        this.body.setVelocityX(-this.speed);
    }

    moveShipRight(rightBoundaryReached: boolean) {
        if (rightBoundaryReached) {
            this.x = staticConfig.width - this.ship.width / 2;

            return;
        }
        this.body.setVelocityX(this.speed);
    }

    moveShipUp(upBoundaryReached: boolean) {
        if (upBoundaryReached) {
            this.y = 0;

            return;
        }

        this.body.setVelocityY(-this.speed);
    }

    moveShipDown(bottomBoundaryReached: boolean) {
        if (bottomBoundaryReached) {
            this.y = staticConfig.height - this.ship.height - this.flame.height;

            return;
        }

        this.body.setVelocityY(this.speed);
    }

    fire = () => {
        const now = Date.now();

        if (now - this.lastBulletTime < this.bulletCooldown) {
            return;
        }

        this.lastBulletTime = now;

        const bullet = new PlayerBullet(this.scene, {
            x: this.x,
            y: this.y,
            speed: this.bulletSpeed * -1,
            power: this.bulletPower,
            texture: 'bullet_P1',
            frame: 'bullet_P1_fr1',
        });

        this.group.add(bullet);

        bullet.move();
    };

    // TODO: Починить скачок спрайта при пересечении любой границы
    checkCursors() {
        const {
            leftBoundaryReached,
            rightBoundaryReached,
            upBoundaryReached,
            bottomBoundaryReached,
        } = this.checkBoundaries();

        this.body.setVelocity(0);

        if (this.cursors.left.isDown || this.stickState.left) {
            this.moveShipLeft(leftBoundaryReached);
        } else if (this.cursors.right.isDown || this.stickState.right) {
            this.moveShipRight(rightBoundaryReached);
        }

        if (this.cursors.up.isDown || this.stickState.up) {
            this.moveShipUp(upBoundaryReached);
        } else if (this.cursors.down.isDown || this.stickState.down) {
            this.moveShipDown(bottomBoundaryReached);
        }

        if (this.cursors.space.isDown || this.xButtonPressed) {
            this.fire();
        }
    }
}
