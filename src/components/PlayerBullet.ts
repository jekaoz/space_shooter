import {Scene} from 'phaser';
import Bullet, {IBullet} from './Bullet';


export default class PlayerBullet extends Bullet {
    constructor(scene: Scene, config: IBullet) {
        const {x, y, speed, texture, frame, power} = config;

        super(scene, {x, y, speed, texture, frame, power});
    }

    explode() {
        if (this.anims.isPlaying) {
            return;
        }

        super.explode();

        this.setOrigin(0.5, 0.5);
        this.once('animationcomplete', () => {
            this.destroy();
        }, this);
        this.play('player_bullet_explode');
    }
}
