import Phaser, {Scene} from 'phaser';
import BaseUIUnit from './BaseUIUnit';
import {
    PI,
    ALIGN,
    EVENTS,
    UI_ALPHA,
    UI_ALPHA_ACTIVE,
} from '../../constants/game';


export interface IStickState {
    left: boolean;
    right: boolean;
    up: boolean;
    down: boolean;
}

const RAD_OFFSET = 0.2;

export default class Stick extends BaseUIUnit {
    startX: number;
    startY: number;

    constructor(scene: Scene) {
        super({
            scene,
            align: `${ALIGN.LEFT}-${ALIGN.BOTTOM}`,
            offsetX: 20,
            offsetY: 20,
            texture: 'stick',
            frame: 'stick_idle',
        });

        this.startX = this.x;
        this.startY = this.y;

        this.setEvents();
    }

    setEvents() {
        this.setInteractive({draggable: true})
            .on(Phaser.Input.Events.DRAG_START, () => {
                this.setFrame('stick_pressed');
                this.setAlpha(UI_ALPHA_ACTIVE);
            })
            // @ts-ignore
            .on(Phaser.Input.Events.DRAG, (_, dragX: number, dragY: number) => {
                const rad = this.getAngle(dragX, dragY);
                const state: IStickState = this.getInitialState();

                if (rad > -RAD_OFFSET && rad <= RAD_OFFSET) {
                    state.right = true;
                    this.setFrame('stick_right');
                } else if (rad > RAD_OFFSET && rad <= (PI / 2 - RAD_OFFSET)) {
                    state.right = true;
                    state.down = true;
                    this.setFrame('stick_right-down');
                } else if (rad > (PI / 2 - RAD_OFFSET) && rad <= (PI / 2 + RAD_OFFSET)) {
                    state.down = true;
                    this.setFrame('stick_down');
                } else if (rad > (PI / 2 + RAD_OFFSET) && rad <= (PI - RAD_OFFSET)) {
                    state.left = true;
                    state.down = true;
                    this.setFrame('stick_left-down');
                } else if (rad > (PI - RAD_OFFSET) || rad <= (RAD_OFFSET - PI)) {
                    state.left = true;
                    this.setFrame('stick_left');
                } else if (rad > (RAD_OFFSET - PI) && rad <= -(PI / 2 + RAD_OFFSET)) {
                    state.left = true;
                    state.up = true;
                    this.setFrame('stick_left-up');
                } else if (rad > -(PI / 2 + RAD_OFFSET) && rad <= -(PI / 2 - RAD_OFFSET)) {
                    state.up = true;
                    this.setFrame('stick_up');
                } else if (rad > -(PI / 2 - RAD_OFFSET) && rad <= -RAD_OFFSET) {
                    state.right = true;
                    state.up = true;
                    this.setFrame('stick_right-up');
                }

                this.emitter.emit(EVENTS.MOVE_STICK, {
                    state,
                    event: Phaser.Input.Events.DRAG,
                });
            })
            .on(Phaser.Input.Events.DRAG_END, () => {
                this.setFrame('stick_pressed');
                setTimeout(() => {
                    this.setFrame('stick_idle');
                    this.setAlpha(UI_ALPHA);
                }, 100);
                this.x = this.startX;
                this.y = this.startY;

                this.emitter.emit(EVENTS.MOVE_STICK, {
                    state: this.getInitialState(),
                    event: Phaser.Input.Events.DRAG_END,
                });
            });
    }

    getInitialState(): IStickState {
        return {
            left: false,
            right: false,
            up: false,
            down: false,
        };
    }

    getAngle(x: number, y: number) {
        return Phaser.Math.Angle.Between(this.startX, this.startY, x, y);
    }
}
