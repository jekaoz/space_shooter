import Phaser, {Scene} from 'phaser';
import EventDispatcher from '../../libs/EventDispatcher';
import {staticConfig} from '../../constants/common';
import {ALIGN, UI_ALPHA, UI_DEPTH} from '../../constants/game';


interface IBaseUIUnit {
    scene: Scene;
    texture: string;
    align?: string;
    frame?: string;
    offsetX?: number;
    offsetY?: number;
}

export default class Joystick extends Phaser.GameObjects.Sprite {
    emitter: EventDispatcher;

    constructor(config: IBaseUIUnit) {
        const {
            scene,
            texture,
            frame,
            align = `${ALIGN.LEFT}-${ALIGN.TOP}`,
            offsetX = 0,
            offsetY = 0,
        } = config;

        const [horAlign, vertAlign] = align.split('-');
        const initialX = horAlign === ALIGN.LEFT ? offsetX : staticConfig.width - offsetX;
        const initialY = vertAlign === ALIGN.TOP ? offsetY : staticConfig.height - offsetY;

        super(scene, initialX, initialY, texture, frame);

        this.emitter = new EventDispatcher();

        this.x += this.width / 2 * (horAlign === ALIGN.LEFT ? 1 : -1);
        this.y += this.height / 2 * (vertAlign === ALIGN.TOP ? 1 : -1);

        this.setAlpha(UI_ALPHA);
        this.setDepth(UI_DEPTH);

        this.scene.add.existing(this);
    }
}
