import Phaser, {Scene} from 'phaser';
import EventDispatcher from '../../libs/EventDispatcher';
import {
    EVENTS,
    SCORE_TEXT_CONFIG,
} from '../../constants/game';


export default class Score extends Phaser.GameObjects.Text {
    emitter: EventDispatcher;
    points: number;

    constructor(scene: Scene, initialValue = '0') {
        super(scene, 8, 2, initialValue, SCORE_TEXT_CONFIG);

        this.emitter = new EventDispatcher();
        this.points = Number(initialValue || '0');

        this.setDepth(1);
        this.setListeners();

        this.scene.add.existing(this);
    }

    setListeners() {
        this.emitter.on(EVENTS.SET_SCORE, this.setScore);
    }

    setScore = (addedPoints: number) => {
        this.points += addedPoints;
        this.text = String(this.points);
    };
}
