import Phaser from 'phaser';
import ParallaxBackground from '../components/ParallaxBackground';
import Score from '../components/UI/Score';
import Stick from '../components/UI/Stick';
import XButton from '../components/UI/XButton';
import Player from '../components/Player';
import Hazard from '../components/Hazard';
import BowHazard from '../components/BowHazard';
import {Layers} from '../typings/game';
import resolveCollisions from '../libs/resolveCollisions';
import Blink from '../shaders/Blink';


export default class L1Scene extends Phaser.Scene {
    cursors: Phaser.Types.Input.Keyboard.CursorKeys;
    playerGroup: Phaser.GameObjects.Group;
    hazardGroup: Phaser.GameObjects.Group;
    pipeline: Phaser.Renderer.WebGL.WebGLPipeline;
    bg: ParallaxBackground;
    player: Player;

    // Демонстрационный моб
    bowHazard: Hazard;

    constructor() {
        super('L1');
    }

    init() {
        this.createUI();

        this.pipeline = (this.game.renderer as Phaser.Renderer.WebGL.WebGLRenderer)
            .addPipeline('blink', new Blink(this.game));

        this.anims.create({
            key: 'unit_explode',
            frameRate: 10,
            frames: this.anims.generateFrameNames('unit_explode'),
            hideOnComplete: true,
        });

        this.anims.create({
            key: 'player_bullet_explode',
            frameRate: 10,
            frames: this.anims.generateFrameNames('player_bullet_explode'),
        });

        this.anims.create({
            key: 'bow_hazard',
            frameRate: 10,
            repeat: -1,
            frames: this.anims.generateFrameNames('bow_hazard'),
        });

        this.cursors = this.input.keyboard.createCursorKeys();
        this.playerGroup = this.add.group();
        this.hazardGroup = this.add.group();
    }

    create() {
        this.bg = new ParallaxBackground(this, [
            {id: 'stars_far_L1', offset: 0.25},
            {id: 'stars_near_L1', offset: 0.5},
        ]);

        this.player = new Player(this, this.cursors, this.playerGroup);
        this.playerGroup.add(this.player);

        // Демонстрационный моб
        this.bowHazard = new BowHazard(this, {
            x: 200,
            y: 250,
            texture: 'bow_hazard',
            speed: 150,
            points: 300,
            layer: Layers.FlyingHazard,
        });

        this.hazardGroup.add(this.bowHazard);

        this.addOverlap();
    }

    createUI() {
        new Score(this);

        if (this.sys.game.device.input.touch) {
            new Stick(this);
            new XButton(this);
        }
    }

    addOverlap() {
        this.physics.add.overlap(
            this.playerGroup,
            this.hazardGroup,
            resolveCollisions,
            null,
            this,
        );
    }

    update() {
        this.bg.move();
        this.player.checkCursors();
        this.playerGroup.children.entries.forEach((entry) => entry?.update());
    }
}
