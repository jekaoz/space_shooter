import Phaser from 'phaser';


export default class BootScene extends Phaser.Scene {
    constructor() {
        super('Boot');
    }
    preload() {
        this.load.image('stars_far_L1', 'assets/background/stars_far_L1.png');
        this.load.image('stars_near_L1', 'assets/background/stars_near_L1.png');
        this.load.image('player_ship_1', 'assets/player/player_ship_1.png');

        this.load.atlas(
            'stick',
            'assets/UI/controls/stick.png',
            'assets/UI/controls/stick.json',
        );
        this.load.atlas(
            'x_button',
            'assets/UI/controls/x_button.png',
            'assets/UI/controls/x_button.json',
        );
        this.load.atlas(
            'bullet_P1',
            'assets/bullets/bullet_P1.png',
            'assets/bullets/bullet_P1.json',
        );
        this.load.atlas(
            'ship_flame',
            'assets/player/player_ship_flame.png',
            'assets/player/player_ship_flame.json',
        );
        this.load.atlas(
            'bow_hazard',
            'assets/hazards/bow_hazard.png',
            'assets/hazards/bow_hazard.json',
        );
        this.load.atlas(
            'player_bullet_explode',
            'assets/bullets/player_bullet_explode.png',
            'assets/bullets/player_bullet_explode.json',
        );
        this.load.atlas(
            'unit_explode',
            'assets/explosions/unit_explode.png',
            'assets/explosions/unit_explode.json',
        );
    }
    create() {
        this.scene.start('L1');
    }
}
